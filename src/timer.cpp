//
// Copyright: (c) 2010-2011 Sam Hocevar <sam@hocevar.net>
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of the Do What The Fuck You Want To
//   Public License, Version 2, as published by Sam Hocevar. See
//   http://sam.zoy.org/projects/COPYING.WTFPL for more details.
//
#include "timer.h"

#include <SDL.h>

Timer::Timer()
    : startCount(SDL_GetPerformanceCounter())
{
}

float Timer::PollMs()
{
    double deltaCount = SDL_GetPerformanceCounter() - startCount;
    return (float)(deltaCount * 1000.0 / SDL_GetPerformanceFrequency());
}

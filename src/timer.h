//
// Lol Engine
//
// Copyright: (c) 2010-2011 Sam Hocevar <sam@hocevar.net>
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of the Do What The Fuck You Want To
//   Public License, Version 2, as published by Sam Hocevar. See
//   http://sam.zoy.org/projects/COPYING.WTFPL for more details.
//

#pragma once

/*!
 * A thing to check elapsed time
 */
class Timer {
public:
    Timer();
    float PollMs();

private:
    unsigned long long startCount; // SDL performance counter is uint64_t
};

/*
 *  Abuse - dark 2D side-scrolling platform game
 *  Copyright (c) 1995 Crack dot Com
 *  Copyright (c) 2005-2011 Sam Hocevar <sam@hocevar.net>
 *
 *  This software was released into the Public Domain. As with most public
 *  domain software, no warranty is made or implied by Crack dot Com, by
 *  Jonathan Clark, or by Sam Hocevar.
 */

#ifndef __EVENT_HPP_
#define __EVENT_HPP_

/* Q: Why are these powers of 2? They're never ORed together... */
#define EV_MOUSE_MOVE     1
#define EV_MOUSE_BUTTON   2
#define EV_KEY            4
/*#define EV_KEY_SPECIAL    8 UNUSED
 #define EV_REDRAW        16 UNUSED */
#define EV_SPURIOUS      32
/* RESIZE is effectively unused (it can never be generated) */
#define EV_RESIZE        64
#define EV_KEYRELEASE   128
#define EV_CLOSE_WINDOW 256
/* DRAG_WINDOW is effectively unused (it CAN be generated, but is never processed) */
#define EV_DRAG_WINDOW  512
#define EV_MESSAGE     1024

#define LEFT_BUTTON    1
#define RIGHT_BUTTON   2
#define MIDDLE_BUTTON  4

#include "keys.h"
#include "sprite.h"

class Jwindow;

class Event : public linked_node
{
public:
    Event()
    {
        type = EV_SPURIOUS;
    }

    Event(int id, char *data)
    {
        type = EV_MESSAGE;
        message.id = id;
        message.data = data;
    }

    int type;
    ivec2 mouse_move;
    int mouse_button, key;

    struct { char alt, ctrl, shift; } key_special;

    Jwindow *window;      // NULL is root
    ivec2 window_position;
    struct { int id; char *data; } message;
};

#endif

#pragma once
#include "common.h"
#include "event.h"

class image;
class palette;
class Sprite;

class EventHandler {
public:
    EventHandler(image* screen, palette* pal);
    virtual ~EventHandler() = default;

    void Push(Event* ev) { m_events.add_end(ev); }

    void SysInit();
    void SysWarpMouse(ivec2 pos);
    void SysEvent(Event& ev);

    int IsPending();
    void Get(Event& ev);
    void flush_screen();

    int has_mouse() { return 1; }
    void SetMouseShape(image* im, ivec2 center)
    {
        m_sprite->SetVisual(im, 1);
        m_center = center;
    }
    void SetMousePos(ivec2 pos)
    {
        m_pos = ivec2(Min(Max(pos.x, 0), m_screen->Size().x - 1), Min(Max(pos.y, 0), m_screen->Size().y - 1));
        SysWarpMouse(m_pos);
    }
    // AR
    ivec2 GetMousePos() { return this->m_pos; }
    void SetIgnoreWheelEvents(bool ignore) { m_ignore_wheel_events = ignore; }
    void SetRightStickCenter(int x, int y)
    {
        m_right_stick_x = x;
        m_right_stick_y = y;
    }
    void SetRightStickMouse() { m_right_stick_x = m_right_stick_y = -1; }

private:
    linked_list m_events;
    int m_pending, last_key;
    bool m_ignore_wheel_events;
    // "Dead zone" before motion of a stick "counts".
    // Maximum stick values are 0x7FFF, currently I've
    // arbitrarily set this to 1/4th.
    int m_dead_zone; // AR (int m_dead_zone = 0x2000;)
    // Scale amount for the right stick when moving the mouse. The range is
    // -0x7FFF to 0x7FFF, or -32767 to 32767. The default means it will move
    // a maximum of 3 pixels per tick.
    int m_right_stick_scale; // AR (int m_right_stick_scale = 0x2000;)
    // Scale amount for the right stick when it's player-locked.
    // 0x400 gives a range of -31 to 31.
    int m_right_stick_player_scale; // AR (int m_right_stick_player_scale = 0x400;)
    int m_right_stick_x, m_right_stick_y;

    image* m_screen;

protected:
    /* Mouse information */
    Sprite* m_sprite;
    ivec2 m_pos, m_center;
    int m_button;
};

//
// Lol Engine
//
// Copyright: (c) 2010-2011 Sam Hocevar <sam@hocevar.net>
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of the Do What The Fuck You Want To
//   Public License, Version 2, as published by Sam Hocevar. See
//   http://sam.zoy.org/projects/COPYING.WTFPL for more details.
//
#pragma once
#include <cmath> // for sqrt

//
// The Matrix classes
// ------------------
//

#define VECTOR_OP(op)                                                                                                  \
  template <typename U> inline Vec2<T> operator op(Vec2<U> const& val) const                                           \
  {                                                                                                                    \
    Vec2<T> ret;                                                                                                       \
    for (int n = 0; n < 2; n++)                                                                                        \
      ret[n] = (*this)[n] op val[n];                                                                                   \
    return ret;                                                                                                        \
  }                                                                                                                    \
                                                                                                                       \
  template <typename U> inline Vec2<T> operator op##=(Vec2<U> const& val) { return *this = (*this)op val; }

#define BOOL_OP(op, op2, ret)                                                                                          \
  inline bool operator op(Vec2<T> const& val) const                                                                    \
  {                                                                                                                    \
    for (int n = 0; n < 2; n++)                                                                                        \
      if (!((*this)[n] op2 val[n]))                                                                                    \
        return !ret;                                                                                                   \
    return ret;                                                                                                        \
  }

#define SCALAR_OP(op)                                                                                                  \
  inline Vec2<T> operator op(T const& val) const                                                                       \
  {                                                                                                                    \
    Vec2<T> ret;                                                                                                       \
    for (int n = 0; n < 2; n++)                                                                                        \
      ret[n] = (*this)[n] op val;                                                                                      \
    return ret;                                                                                                        \
  }                                                                                                                    \
                                                                                                                       \
  inline Vec2<T> operator op##=(T const& val) { return *this = (*this)op val; }

template <typename T> struct Vec2 {
  T x;
  T y;

  inline Vec2() {}
  inline Vec2(T val) { x = y = val; }
  inline Vec2(T _x, T _y)
  {
    x = _x;
    y = _y;
  }

  inline T& operator[](int n) { return *(&x + n); }
  inline T const& operator[](int n) const { return *(&x + n); }

  VECTOR_OP(-)
  VECTOR_OP(+)
  VECTOR_OP(*)
  VECTOR_OP(/)

  BOOL_OP(==, ==, true)
  BOOL_OP(!=, ==, false)
  BOOL_OP(<=, <=, true)
  BOOL_OP(>=, >=, true)
  BOOL_OP(<, <, true)
  BOOL_OP(>, >, true)

  SCALAR_OP(-)
  SCALAR_OP(+)
  SCALAR_OP(*)
  SCALAR_OP(/)

  template <typename U> inline operator Vec2<U>() const
  {
    Vec2<U> ret;
    for (int n = 0; n < 2; n++)
      ret[n] = static_cast<U>((*this)[n]);
    return ret;
  }

  inline Vec2<T> operator-() const
  {
    Vec2<T> ret;
    for (int n = 0; n < 2; n++)
      ret[n] = -(*this)[n];
    return ret;
  }

  inline T sqlen() const
  {
    T acc = 0;
    for (int n = 0; n < 2; n++)
      acc += (*this)[n] * (*this)[n];
    return acc;
  }

  inline float len() const
  {
    using namespace std;
    return sqrtf((float)sqlen());
  }
};

typedef Vec2<float> vec2;
typedef Vec2<int> ivec2;

#define SCALAR_GLOBAL(op, U)                                                                                           \
  template <typename T> static inline Vec2<U> operator op(U const& val, Vec2<T> const& that)                           \
  {                                                                                                                    \
    Vec2<U> ret;                                                                                                       \
    for (int n = 0; n < 2; n++)                                                                                        \
      ret[n] = val op that[n];                                                                                         \
    return ret;                                                                                                        \
  }

#define SCALAR_GLOBAL2(op)                                                                                             \
  SCALAR_GLOBAL(op, int)                                                                                               \
  SCALAR_GLOBAL(op, float)

SCALAR_GLOBAL2(-)
SCALAR_GLOBAL2(+)
SCALAR_GLOBAL2(*)
SCALAR_GLOBAL2(/)

//
// Custom utility functions
//
template <typename T> inline T Min(T a, T b) { return a < b ? a : b; }

template <typename T> inline T Max(T a, T b) { return a > b ? a : b; }

static inline ivec2 Min(ivec2 a, ivec2 b) { return ivec2(Min(a.x, b.x), Min(a.y, b.y)); }
static inline ivec2 Max(ivec2 a, ivec2 b) { return ivec2(Max(a.x, b.x), Max(a.y, b.y)); }
